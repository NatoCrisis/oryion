source 'https://rubygems.org'

gem 'rails', '~> 5.1', '>= 5.1.2'
gem 'pg', '~> 0.18.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2', '>= 4.2.2'

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails', '~> 6.0', '>= 6.0.1'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc


gem 'puma', '~> 3.8', '>= 3.8.1'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'byebug'
  gem 'rubocop', '~> 0.49.1'
  gem 'pry-byebug'
  gem 'database_cleaner'
  gem 'rspec-rails', '~> 3.0'
  gem 'capybara', '~> 2.14' , '>= 2.14.4'
  gem 'selenium-webdriver', '~> 2.48', '>= 2.48.1'
  gem 'headless', '~> 2.3', '>= 2.3.1'
  gem 'poltergeist', '~> 1.15'
  gem 'factory_girl', '~> 4.8'
  gem 'capybara-select2', '~> 1.0', '>= 1.0.1'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  
  gem 'rubycritic', '~> 3.2', '>= 3.2.3'
  gem 'brakeman', '~> 3.7'
end

gem 'devise', '~> 4.3'
gem 'pundit', '~> 1.1'
gem 'bootstrap', '~> 4.0.0.alpha6'
gem 'kaminari', '~> 1.0', '>= 1.0.1'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.2'
gem 'select2-rails', '~> 4.0', '>= 4.0.3'
gem 'textacular', '~> 5.0'
gem 'bootstrap-switch-rails', '~> 3.3', '>= 3.3.3'
gem 'redcarpet', '~> 3.4'
gem 'coderay', '~> 1.1', '>= 1.1.1'