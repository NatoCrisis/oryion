#TODO:: look at https://github.com/teamcapybara/capybara#asynchronous-javascript-ajax-and-friends
module WaitForAjax
   def wait_for_ajax
      Timeout.timeout(Capybara.default_max_wait_time) do
          loop until finished_all_ajax_requests?
      end
   end
   
   def finished_all_ajax_requests?
      jquery_active = page.evaluate_script('jQuery.active').zero? 
      #sleep(0.75.seconds)
      #puts jquery_active
      jquery_active
   end
end