module Select2Helper
    
    def click_select2
        find('span.select2').click
    end
    
    def set_select2_text text
        find(:xpath, "//body").find(".select2-search input.select2-search__field").set(text)
        page.execute_script(%|$("input.select2-search__field:visible").keyup();|)
    end
    
    def click_select2_result result
        find(:xpath, "//body").find(".select2-results li.select2-results__option", text: result).click
    end
end