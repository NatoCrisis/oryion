require 'rails_helper'

describe 'navigate' do
    
    #TODO:: make the user log in
    #TODO:: make the user an admin
    
    let(:user) do
       FactoryGirl.create(:user) 
    end
    
    let(:admin_user) do
        FactoryGirl.create(:admin_user) 
    end
    
    let(:project) do
        Project.create!(name: project_name, description:'Project description', created_by: admin_user)
    end
    
    let(:second_project) do
         FactoryGirl.create(:second_project) 
    end
    
    let(:project_name) do
        "This is a test project #{ Random.new_seed }"
    end
    
    describe 'index' do

        before do
            login_as(user)
            project
            visit projects_path 
        end
    
        it 'can be reached successfully' do
            expect(page.status_code).to eq(200) 
        end
       
        it 'has a list of posts', js: false do
            expect(page).to have_text(Project.last.name)
        end
        
        it 'shows only active projects by default', js: false do
            Project.last.update!(status: 1)
            visit projects_path
            expect(page).not_to have_text(Project.last.name)
        end
        
        it 'shows archived projects when selected', js: false do
            Project.last.update!(status: 1)
            
            select('Archived', :from => 'project-status')
            wait_for_ajax
            expect(page).to have_text(Project.last.name)
        end
    end
    
    describe 'create' do
        
        before do
            login_as(admin_user)
            visit projects_path
            click_button "Create" 
        end
        
        it 'can close modal' , js: true do
            click_on 'modal-close'
            expect(page).to have_selector('#create-modal', visible: false)
        end
        
        it 'can create base project', js: true do
            pn = project_name
            
            fill_in 'project[name]', with: pn
            fill_in 'project[description]', with: 'This is a description'
        
            click_on 'modal-create'
            
            wait_for_ajax
            
            
            # This first expect is rather pointless, but it will force
            #  Capybara to wait for all ajax to complete
            expect(page).to have_xpath("//input[@value='#{pn}']")
            expect(current_path).to eq(edit_project_path(Project.last))
        end
        
        it 'has created by set to current user' , js: true do
            fill_in 'project[name]', with: project_name
            fill_in 'project[description]', with: 'This is a description'
        
            click_on 'modal-create'
            
            wait_for_ajax
            
            expect(page).to have_content(admin_user.name)
        end
        
       it 'fails when name is missing' , js: true do
            
            fill_in 'project[description]', with: 'This is a description'
            
            click_button 'modal-create'
            wait_for_ajax
            
            expect(page).to have_content("Name can't be blank")
        end
        
        it 'fails when description is missing' , js: true do
            
            fill_in 'project[name]', with: 'Test project'
            
            click_button 'modal-create'
            
            wait_for_ajax
            
            expect(page).to have_text("Description can't be blank")
        end
        
        it 'fails when name is already used', js: true do
          p_name = project.name
          
          fill_in 'project[name]', with: p_name
          fill_in 'project[description]', with: 'This is a description'
        
          click_on 'modal-create'
          wait_for_ajax
          expect(page).to have_text("Name has already been taken")
            
        end
    end
    
    describe 'edit' do
       before do
           login_as(admin_user)
           visit edit_project_path(project)
       end
       
       it 'can reach project edit page', js: true do
           expect(page.status_code).to eq(200) 
       end
       
       it 'can edit name', js: true do
          find(:id, 'project[name]').click
          
          fill_in('project[name]', :with => 'Different name')
          
          find("span.inline-save-button").click
          
          wait_for_ajax
          
          visit edit_project_path(project)
          expect(page).to have_xpath("//input[@value='Different name']")
       end
       
       it 'fails when changing to already used name', js: true do
          
          find(:id, 'project[name]').click
          
          fill_in('project[name]', :with => second_project.name)
          
          find("span.inline-save-button").click
          
          wait_for_ajax
          
          expect(page).to have_text('Name has already been taken')
       end
       
       it 'can edit description', js: true do
          find(:id, 'project[description]').click
          
          fill_in('project[description]', :with => 'Huzza')
          
          find("span.inline-save-button").click
          
          wait_for_ajax

          visit edit_project_path(project)
          expect(page).to have_xpath("//input[@value='Huzza']")
       end
       
       it 'can edit project owner', js: true do
           name = user.name
           
           click_select2
           
           expect(page).to have_content('Please enter 2 or more characters')
           
           set_select2_text 'test'
           
           expect(page).to have_content(name)
           
           click_select2_result name
           
           project.reload
           
           wait_for_ajax
           
           visit edit_project_path(project)
           
           expect(page).to have_text name
       end
       
       #TODO
       xit 'can remove project owner', js: true do
           project.update!(project_owner: User.last)
           visit edit_project_path(project)
           
           find(:xpath, "//span[@class='select2-selection__clear']").click
           
           project.reload
           
           wait_for_ajax
           
           visit edit_project_path(project)
           
           expect(page).to have_text name
       end
       
       it 'can change project to inactive', js: true do
          
          find(:xpath, "//div[@id='switch-project[status]']").click
          
          project.reload
          
          visit edit_project_path(project)
          
          expect(page).to have_text 'Archived'

       end
       
       it 'can edit profile', js: true do
           new_profile = Random.new_seed
           
           find(:xpath, "//div[@id='placeholder-project[profile]']").click
           
           fill_in('project[profile]', :with => new_profile.to_s)
           
           find(:xpath, "//button[@class='btn btn-success btn-sm inline-save-button']").click
           
           wait_for_ajax
           
           project.reload
           
           visit edit_project_path(project)
           
           expect(page).to have_text new_profile.to_s
       end
       
       it 'can set iterations to autostart', js: true do
           wait_for_ajax
           find(:xpath, "//div[@id='switch-project[auto_start_iter]']").click
           project.reload
           visit edit_project_path(project)
           expect(page).to have_text 'On'
       end
       
       it 'can set iteration length', js: true do
           find('project[iter_length]').find(:xpath, 'option[1]').select_option
           project.reload
           visit edit_project_path(project)
           expect(page).to have_text '1'
       end
    end
    
    
    describe 'destroy' do
        before do
           login_as admin_user
           project
           visit projects_path
        end
        
        it 'can delete a project' , js: false do
            
            Project.create!(name: 'bonko', description: 'huzza', created_by: user)
            
            visit edit_project_path(Project.last)
            
            click_button 'destroy-project'
            
            within(:xpath, "//div[@id='delete-section']") do
                fill_in 'project[name]', with: 'bonko'
                click_button 'delete-project'    
            end
            
            # TODO:: Not really sure why, but this raises a Capybara error.
            #  The actual app seems to work fine and redirect back
            #  to the projects_path, so for the time being I will leave this out
            #  and simply go to the index page and check that the project was deleted
            #expect(current_path).to eq(projects_path)
            
            visit projects_path
            expect(page).not_to have_text('bonko')
        end
        
        it 'fails to destroy when name is blank', js: false do
            
            Project.create!(name: 'bonko', description: 'huzza', created_by: user)
            
            visit edit_project_path(Project.last)
            
            click_button 'destroy-project'
            
            click_button 'delete-project'
            
            expect(page).to have_text('You must enter the name to delete this project')
        end
        
        it 'fails to destroy when name is incorrect', js: false do
            Project.create!(name: 'bonko', description: 'huzza', created_by: user)
            
            visit edit_project_path(Project.last)
            
            click_button 'destroy-project'
            within(:xpath, "//div[@id='delete-section']") do
                fill_in 'project[name]', with: 'bubbles'
                click_button 'delete-project'    
            end
            
            expect(page).to have_text('Incorrect name')
        end
       
    end
end