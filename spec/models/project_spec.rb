require 'rails_helper'
require 'spec_helper'
require 'project_helper'

require 'json'

describe Project, :type => :model do

    let(:project) do
        FactoryGirl.create(:project) 
    end
        
    describe 'profile markdown' do
        
        it 'has profile_markdown method' do
            p = project
            p.update!(profile: 'blah blah blah')
            
            expect(p.profile_markdown).to eq("<p>blah blah blah</p>\n")
        end
        
        it 'raises exception when attribute is missing' do
            expect{ project.huzza_unto_thee_markdown }.to raise_error(ProjectHelper::NoAttributeError)
        end
   
        # A bit silly to test this, but because I am overriding the method_missing method
        # => I want to make sure that this is still raised correctly when a markdown method
        # => is called
        it 'raises exception when method is missing' do
            expect { project.like_a_boss }.to raise_error(NoMethodError)
        end
    end
    
    describe 'custom json' do

        it 'has correct values from custom json' do
            p = project
            p.update!(profile: 'blah')
        
            json_hash = JSON.parse(p.to_json)
        
            expect(p.id).to eq(json_hash['id'])
            expect(p.name).to eq(json_hash['name'])
            expect(p.description).to eq(json_hash['description'])
            expect(p.status).to eq(json_hash['status'])
            expect(p.profile).to eq(json_hash['profile'])
            expect(p.profile_markdown).to eq(json_hash['profile_markdown'])
        end
    end
end