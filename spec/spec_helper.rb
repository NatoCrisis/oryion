require 'spec_helper'
require 'rspec/rails'

require 'support/wait_for_ajax'
require 'support/select2_helper'
require 'support/controller_macros'
require 'capybara/poltergeist'

require 'devise'

FactoryGirl.definition_file_paths = [File.expand_path('../factories', __FILE__)]
FactoryGirl.find_definitions

Capybara.default_max_wait_time = 5
Capybara.register_driver :poltergeist do |app|
  options = { js_errors:  false }
  Capybara::Poltergeist::Driver.new(app, options)
end

Capybara.javascript_driver = :poltergeist
Capybara.default_driver = :poltergeist

RSpec.configure do |config|

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.before(:suite) do
    #%x[bundle exec rake assets:precompile]
  end

  config.before(:each) do
    # stub_const("SmsTool", FakeSms)
  end
  
  config.include WaitForAjax, type: :feature
  config.include Select2Helper, type: :feature
  #config.include Devise::Test::ControllerHelpers, type: :controller
  config.include Devise::TestHelpers, type: :controller
  config.extend ControllerMacros, :type => :controller
  
  config.include FactoryGirl::Syntax::Methods
  
  config.shared_context_metadata_behavior = :apply_to_host_groups

end
