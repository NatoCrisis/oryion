require 'rails_helper'
require 'json'

describe UsersController, :type => :controller do
    login_user
    
    describe 'search' do
        
        let(:user) do
           FactoryGirl.create(:user) 
        end
        
        let(:second_user) do
           FactoryGirl.create(:second_user) 
        end
        
        let(:third_user) do
           FactoryGirl.create(:third_user) 
        end
        
        it 'returns one user',  js: false do
            get :search, :params => { :email => 'test' }, :format => :json
            found_users = JSON.parse(response.body)
            
            expect(found_users[0]['email']).to eq(subject.current_user.email)
        end
    end
end