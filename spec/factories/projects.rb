FactoryGirl.define do
    
    sequence :proj_name do |n|
        "Test project #{n}" 
    end
    
    factory :project do
        name { generate :proj_name }
        description 'This is a project'
        status 0
        created_by { FactoryGirl.create(:user) }
    end
    
    factory :second_project, class: Project do
       name { generate :proj_name } 
       description 'This is a project'
       status 0
       created_by { FactoryGirl.create(:user) }
    end
    
end