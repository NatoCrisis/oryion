FactoryGirl.define do
    
    sequence :email do |n|
        "test#{n}@example.com"
    end
    
    sequence :u_name do |n|
        "Name #{n}" 
    end
    
    factory :user do
        email { generate :email }
        password '123456'
        name { generate :u_name }
    end

    factory :second_user, class: User do
        email { generate :email }
        password '123456'
        name { generate :u_name }
    end
    
    factory :third_user, class: User do
        email { generate :email }
        password '123456'
        name { generate :u_name }
    end
    
    factory :admin_user, class: AdminUser do
       email 'admin@test.com' 
       password '123456'
       name 'Admin'
    end
    
end