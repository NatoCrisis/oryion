module ApplicationHelper
    include InlineUpdateHelper
    
    def admin?
       current_user.type == 'AdminUser' 
    end
    
    def nav_helper
       nav_links = ''
       
       nav_items.each do |nav|
          nav_links << "<li class='#{ active? nav[:url] }'><a href='#{ nav[:url] }'><span>#{ nav[:title] }</span></a></li>" 
       end
       
       nav_links.html_safe
    end
    
    private 
    
    def active? path
       "active" if current_page? path 
    end
    
    def nav_items
       [
           {
               url: root_path,
               title: "Home"
           },
           {
               url: projects_path,
               title: "Projects"
           }
        ] 
    end
end
