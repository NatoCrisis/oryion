module InlineUpdateHelper
    include ActionView::Helpers::CaptureHelper

    def inline_update_text_field( form, obj_name, obj_val, label_len=2, val_len=10 )
        text_id = "#{form.object_name}[#{obj_name}]"
inline_update = <<-HTML
#{ form.label(obj_name) }
    <div class='input-group'>
        <div id='prev_#{ text_id }' style='display:none;'>#{ obj_val }</div>
        #{ form.text_field(obj_name, id: text_id, class: 'form-control inline-text-field') }
        #{ input_group_helper }
    </div>
HTML
        
        inline_update.html_safe
    end
    
    def inline_update_select2(form, obj_name, placeholder, search_url, search_params, &block)
        option = capture(&block)
        
inline_update = <<-HTML
#{ form.label(obj_name) }

  <div class='form-group'>
     <select id='owned_by' name='project[owned_by_id]' 
        class='inline-select2'
        data-url='#{ search_url }'
        data-placeholder='#{ placeholder }'
        data-search-params='#{ search_params }' >
    
        #{ option }
    
    </select>
  </div>
HTML
        inline_update.html_safe
    end
    
    def inline_switch form, obj_name, current, enum
        zero_key, one_key = enum.keys
        
inline_update = <<-HTML
#{ form.label(obj_name) }

      <div class='form-group'>
        <input type='checkbox'
            id='#{ form.object_name }[#{ obj_name }]'
            class='inline-switch'
            data-on-color='primary'
            data-off-color='warning'
            data-on-text='#{ zero_key.capitalize }'
            data-off-text='#{ one_key.capitalize }'
            data-on-value='#{ zero_key }'
            data-off-value='#{ one_key }'
            
            #{ current == zero_key ? 'checked' : '' }>
            
        <input type='hidden' name='#{ form.object_name }[#{ obj_name }]' 
        value=#{ current } />  
      </div>
      
HTML

        inline_update.html_safe
    end
    
    def inline_markdown form, obj_name, to_markdown, rows = 15
    text_area_id = "#{form.object_name}[#{obj_name}]"
inline_update = <<-HTML
#{ form.label(obj_name) }
<div class='markdown-placeholder' 
     id='placeholder-#{ form.object_name }[#{ obj_name }]'>#{ to_markdown }</div>
     
<div class="btn-group pull-right" role="group" style='display:none;'>
    <button type="button" class="btn btn-success btn-sm inline-save-button">
        #{ fa_icon('check-square-o') }
    </button>
    <button type="button" class="btn btn-danger btn-sm inline-cancel-button">
        #{ fa_icon('window-close') }
    </button>
</div>
<div id='prev_#{ text_area_id }' style='display:none;'>#{ to_markdown }</div>
#{ form.text_area(obj_name, id:text_area_id,  class: 'form-control inline-markdown', rows: rows ) }
HTML

        inline_update.html_safe
    end
    
    def inline_dropdown(form, obj_name, obj_val, options)
        opts = options_for_select(options, obj_val.to_s)

inline_update = <<-HTML
#{ form.label(obj_name) }
<div class='form-group'>
#{ form.select(obj_name, opts, {}, { class: 'form-control inline-dropdown' }) }
</div>
HTML

        inline_update.html_safe
    end

    private
    
    def label_helper form, obj_name, label_len
label = <<-HTML
<div class='control-group'>        
    #{ form.label(obj_name, class: 'control-label') }        
</div>
HTML
    label.html_safe
    end
    
    def input_group_helper
input_group = <<-HTML
<div class='btn-group'>
    <span class='input-group-addon inline-save-button bg-success'>#{ fa_icon('check-square-o') }</span>
    <span class='input-group-addon inline-cancel-button bg-danger'>#{ fa_icon('window-close') }</span>
</div>
HTML

    input_group.html_safe
    end
    
end