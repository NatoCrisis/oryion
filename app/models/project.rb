class Project < ApplicationRecord
    include ProjectHelper
    
    before_create :create_as_active, :create_as_auto_start_iter_off
    
    belongs_to :created_by, class_name: 'User', foreign_key: 'created_by_id'
    belongs_to :owned_by, class_name: 'User', foreign_key: 'owned_by_id'
    
    enum status: { active: 0, archived: 1 }
    enum auto_start_iter: { off: 0, on: 1 }
    
    validates_presence_of :name
    validates :name, uniqueness: true
    
    validates_presence_of :description
    
    def as_json option={}
       {
           id: id,
           name: name,
           status: status,
           description: description,
           profile: profile,
           profile_markdown: profile_markdown,
           auto_start_iter: auto_start_iter,
           iter_length: iter_length
       } 
    end
    
    private
    
    def create_as_active
       self.status = 0 
    end
    
    def create_as_auto_start_iter_off
       self.auto_start_iter = 0 
    end
    
end
