require 'active_support/concern'

module ProjectHelper extend ActiveSupport::Concern
    include MarkdownHelper
    
    class NoAttributeError < RuntimeError
    end
    
    def method_missing(method_name, *args, &block)
       # cast the method to a string here so that we are not casting it all over the place
       m_name = method_name.to_s
       split_name = m_name.split('_')
       
       if split_name.last == 'markdown'
           return attribute_markdown m_name
       end
       
       raise NoMethodError.new("undefined method `#{ method_name }` for #{ self }")
    end
    
    private 
    
    def attribute_markdown m_name
        # Get the attribute that we want to update
        # => eg: `big_string_markdown` becomes `big_string`
        attribute = m_name[0, m_name.rindex('_')]
           
        if self.has_attribute? attribute
            to_markdown = self.attributes[attribute]
            return markdown(to_markdown.nil? ? '' : to_markdown)
        else
            raise NoAttributeError.new("no attribute named `#{ attribute }` for #{ self }")
        end
    end
end