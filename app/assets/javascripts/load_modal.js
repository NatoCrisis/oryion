var Modal = (function($){
    
    // Init on doc ready
    $(function() {
        initEventHandlers();
    })
    
    // Init on turbolinks:load
    $(document).on('turbolinks:load', function() {
        initEventHandlers();
    });
    
    function initEventHandlers() {
        $('body').on('show.bs.modal', loadContent);
        $('body').on('click', '#modal-create', submitForm);
        $('body').on('ajax:success','#create-modal', onSuccess);
        $('body').on('ajax:error', '#create-modal',onFail);
    }
    
    function loadContent(event) {
        var modalUrl = $(event.relatedTarget).data('url');
        var urlMethod = $(event.relatedTarget).data('method')
        
        $.ajax({
            url: modalUrl,
            method: urlMethod,
            dataType: 'json',
            contentType: "application/json",
            data: {}
        }).done(function( resp ){ 
            $(event.target).find("div.modal-content").html(resp.html)
        });
    }
    
    function onSuccess(event, data, status, xhr) {
        window.location = data.location;
    }
    
    function onFail(event, xhr, status, error) {
        var errors = xhr.responseJSON;
        $(errors).each(function(idx, error) {
            $('#error-list').append('<li>' + error + '</li>')
        });
        $('#errors').show();
    }
    
    function submitForm(event) {
        var form = $('div.modal-body').find('form');
        $(form).submit();
    }
    
})(jQuery);