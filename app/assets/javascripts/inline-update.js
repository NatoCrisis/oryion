var InlineUpdate = (function($) {

    var updateName;
    
    $(function() {
        initEventHandlers();
    })
    
    function showInputGroup() {
        $(this).parent().find('.input-group-addon').show()
    }
    
    function hideInputGroup() {
    }

    function updateField() {
        var formGroupContainer = $(this).closest('.form-group');
        updateName = $(formGroupContainer).find('input.inline-text-field').attr('name')
        var form = $(formGroupContainer).closest('form');
        $(form).submit();  
    }
    
    function cancelUpdate() {
        var parent = $(this).parent()
        var oldValue = $(parent).find('div[id*="prev_"]').html()
        $(parent).find('input[type="text"]').val(oldValue);
        $(this).parent().find('.input-group-addon').hide()
    }
    
    function updateSuccess(xhr, object, err) {
        var elementUpdated = $('.form-control[name="' + updateName + '"]');
        var formGroupContainer = $(elementUpdated).closest('.form-group');
        
        $(formGroupContainer).find('div[id$="prev_"]').html($(elementUpdated).val());
        $(formGroupContainer).find('.btn-group').hide();
        $(formGroupContainer).find('div.form-control-feedback').remove();
        
        $("body").trigger({type: 'inline-save.success', elementUpdated: elementUpdated});
        
        $(elementUpdated).blur();
    }
    
    function updateFailure(xhr, object, err) {
        var elementUpdated = $('.form-control.inline-text-field[name="' + updateName + '"]');
        var displayError = object.responseJSON[0]
        $(elementUpdated).parent().after("<div class='form-control-feedback'>" + displayError + "</div>")
    }
    
    function initEventHandlers() {
        $('body').on('click', '.inline-text-field', showInputGroup)
        $('body').on('click', 'span.inline-save-button', updateField);
        $('body').on('click', 'span.inline-cancel-button',cancelUpdate)
        $('body').on('ajax:success', 'form.inline-form', updateSuccess);
        $('body').on('ajax:error', 'form.inline-form', updateFailure);        
    }
    
    function setToUpdate(name) {
        updateName = name;
    }
    
    function submitForm(element) {
        var form = $(element).closest('form.inline-form');
        $(form).submit()
    }
    
    return {
        toUpdate: setToUpdate,
        update: updateField,
        submitForm: submitForm
    }
})(jQuery);

// INLINE MARKDOWN
(function($){
    $(function() {
        initEventHandlers();
    });
    
    function showInputGroup() {
        $(this).parent().find('.markdown-placeholder').hide()
        $(this).parent().find('.btn-group').show()
        $(this).parent().find('textarea.inline-markdown').show()
    }
    
    function cancelUpdate() {
        var formGroupContainer = $(this).closest('.form-group');
        
        var oldValue = $(formGroupContainer).find('div[id*="prev_"]').html()
        $(formGroupContainer).find('div.markdown-placeholder').html(oldValue);
        $(formGroupContainer).find('textarea.inline-markdown').val(oldValue);
        
        $(formGroupContainer).find('div.markdown-placeholder').show();
        $(formGroupContainer).find('textarea.inline-markdown').hide();
        $(formGroupContainer).find('div.btn-group').hide();
    }
    
    function update() {
        var formGroupContainer = $(this).closest('.form-group');
        
        InlineUpdate.toUpdate($(formGroupContainer).find('textarea.inline-markdown').attr('name'));
        
        var form = $(formGroupContainer).closest('form');
        $(form).submit();
    }
    
    function updateSuccess(event) {
        var element = event.elementUpdated;
        if($(element).is(':visible')) {
            var formGroupContainer = $(element).closest('.form-group');
            $(formGroupContainer).find('div.markdown-placeholder').show();
            $(element).hide();
        }
    }
    
    function updateFailure(xhr, object, err) {
        var elementUpdated = $('.form-control.inline-text-field[name="' + InlineUpdate.updateName + '"]');
        var displayError = object.responseJSON[0]
        $(elementUpdated).parent().after("<div class='form-control-feedback'>" + displayError + "</div>")
    }
    
    function initEventHandlers() {
        $('body').on('click', '.markdown-placeholder', showInputGroup)
        $('body').on('click', 'button.inline-save-button', update);
        $('body').on('click', 'button.inline-cancel-button',cancelUpdate)
        $('body').on('inline-save.success', updateSuccess );
    }
})(jQuery);

// INLINE SELECT 2
(function($){
    // Init elements on doc ready
    $(function() {
        $(document).find('select.inline-select2').each(function(i, element) {
            initSelect2(element);
        })
    })
    
    // Init elements on turbolinks:load
    $(document).on('turbolinks:load', function() {
        $(document).find('select.inline-select2').each(function(i, element) {
            initSelect2(element);
        })
    });
    
    function initSelect2(element) {
      var inlineSelect = $(element).select2({
        width: '100%',
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            url: function() {
                return $(this).attr('data-url')
            },
            dataType: 'json',
            delay: 250,
        
            data: function (params) {
                var searchParam = $(this).attr('data-search-params').split(',')
                
                var searchData = {};
                $.each(searchParam, function(idx, elem) {
                    searchData[elem] = params.term;    
                });
                
                return searchData 
            },
            processResults: function(data, params) {
                return {
                    results: data.map(function(val, idx, arr) {
                        return { id: val.id, text: val.name}
                    })
                }
            }
      }  
    
    })
    
    inlineSelect.on('select2:selecting', function(event) {
        InlineUpdate.toUpdate($(inlineSelect).attr('name'));
    });
    
    inlineSelect.on('select2:unselecting', function(event) {
        InlineUpdate.toUpdate($(inlineSelect).attr('name'));
        $(this).val('0')
    });
    
    inlineSelect.on('change', function(event) {
        console.log('change')
        var form = $('body').find('form.inline-form');
        $(form).submit();
    });
    }
    
})(jQuery);

// INLINE DROPDOWN
(function($){
    $(function() {
        $(document).find('.inline-dropdown').each(function(i, element) {
            initDropdown(element);
        })
    });
    
    $(document).on('turbolinks:load', function() {
        $(document).find('.inline-dropdown').each(function(i, element) {
            initDropdown(element);
        })
    });
    
    function initDropdown(element) {
        $(element).on('change', function(event) {
            InlineUpdate.toUpdate($(this).attr('name'));
            InlineUpdate.submitForm(element);
        })
    }
    
})(jQuery);

// INLINE SWITCH
(function($){
    
    $(function() {
        $(document).find('.inline-switch').each(function(i, element) {
            initSwitch(element);
        })
    });
    
    $(document).on('turbolinks:load', function() {
        $(document).find('.inline-switch').each(function(i, element) {
            initSwitch(element);
        })
    });
    
    function initSwitch(element) {
        var s = $(element).bootstrapSwitch();
        
        var id = $(s).attr('id')
        $(s).closest('div.bootstrap-switch').attr('id', 'switch-' + id)
        
        $('.inline-switch').on('switchChange.bootstrapSwitch',function(event, state) {
            
            var form = $('body').find('form.inline-form');
            
            var id = $(this).attr('id');
            var hidden = $('input[name="' + id + '"]');
            
            InlineUpdate.toUpdate($(hidden).attr('name'));
            
            var onVal = $(this).attr('data-on-value');
            var offVal = $(this).attr('data-off-value');
            
            $(hidden).val( (state ? onVal : offVal) )
            
            $(form).submit();
        });
    }
    
})(jQuery);

