class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  include Pundit
  protect_from_forgery with: :exception
  
  layout :layout_by_resource
  
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  
  def after_sign_in_path_for resource
    root_path
  end
  
  def after_sign_out_path_for(resource_or_scope)
    flash.clear
    new_user_session_path
  end
  
  private
  
  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action"
    redirect_to(root_path)
  end  
  
  def layout_by_resource
    if devise_controller?
      'session'
    else
      'application'
    end
  end
end
