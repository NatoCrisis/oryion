class ProjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  def index
    
    if params[:status].blank? || params[:status] == 'active'
      @projects = Project.active.page(params[:page]).per(10)
    elsif params[:status] == 'archived'
      @projects = Project.archived.page(params[:page]).per(10)
    end
    
  end

  def show
  end

  def new
    @project = Project.new
    
    toReturn = ( render_to_string :partial => "modal_new", :layout => false, :locals => {:project => @project})
    respond_to do |format|
      format.json { render :layout => false, :json => {:html => toReturn }  } #:json => {:html => toReturn }
    end
  end

  def edit
    begin
      authorize @project
    rescue Pundit::NotAuthorizedError
      redirect_to @project
    end
  end

  def create
    @project = Project.new(project_params)
    @project.created_by = current_user

    respond_to do |format|
      if @project.save
        format.json { render :layout => false, :json => { :location => edit_project_path(@project) }}
        format.html { redirect_to edit_project_path(@project), notice: 'Project was successfully created.' }
      else
        format.json { render json: @project.errors.full_messages.to_json, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @project.update(project_params)
        #format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render json: @project, status: :ok}
      else
        #format.html { render :edit }
        format.json { render json: @project.errors.full_messages.to_json, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if project_params[:name] == @project.name
      @project.destroy
      redirect_to projects_path, notice: 'Project was successfully destroyed.' 
    elsif project_params[:name].blank?
      respond_to do |format|
        format.json { render json: 'You must enter the name to delete this project', status: :unprocessable_entity }
      end  
    else
      respond_to do |format|
        format.json { render json: 'Incorrect name', status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(
        :name, :description, :status, 
        :owned_by_id, :profile, :auto_start_iter,
        :iter_length )
    end
end
