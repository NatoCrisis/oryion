class UsersController < ApplicationController
   before_action :authenticate_user!
   include SetSearchLimitConcern
   
   def search
       respond_to do |format|
          results = fuzzy_search
          format.json { render json: results, status: :ok } 
       end
   end
   
   def index
   end
   
   def show
   end

   private 
   
   def fuzzy_search
      if !search_params.empty?
         User.fuzzy_search(search_params)
      else
         User.new
      end
   end
   
   def search_params
      params.permit(:email, :name).to_h
   end

end