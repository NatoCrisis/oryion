module SetSearchLimitConcern extend ActiveSupport::Concern
   
   included do
       before_action :set_limit, only: [:search]
   end
   
   def set_limit
       ActiveRecord::Base.connection.execute("SELECT set_limit(0.1);")
   end
    
end