class ProjectPolicy < ApplicationPolicy

    def edit?
        admin? || record.owned_by_id == user.id  
    end
    
    private
    
    def admin?
        user.type == 'AdminUser'
    end
end