class AddProfileToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :profile, :text
  end
end
