class AddOwnedByToProject < ActiveRecord::Migration[5.0]
  def change
    add_reference :projects, :owned_by, foreign_key: { to_table: :users }, index: true
  end
end
