class AddAutoStartIterationToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :auto_start_iter, :int
  end
end
