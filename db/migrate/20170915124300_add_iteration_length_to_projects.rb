class AddIterationLengthToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :iter_length, :int
  end
end
