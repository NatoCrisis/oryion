class AddCreatedByToProject < ActiveRecord::Migration[5.0]
  def change
    add_reference :projects, :created_by, foreign_key: { to_table: :users }, index: true
  end
end
