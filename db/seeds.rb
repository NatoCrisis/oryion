AdminUser.create!(name: 'Admin User', email: 'admintest@test.com', password: '123456', password_confirmation: '123456')
puts 'Created admin user'

(1..10).each do |n|
    User.create!(name: "User #{n}", email: "test#{n}@test.com", password: '123456', password_confirmation: '123456')
end
puts 'Created 10 users'

(1..20).each do |n|
    Project.create!(name: "Project #{n}", description: 'Description', created_by: User.last  )
end
puts 'Created projects'