Rails.application.routes.draw do
  resources :projects
  #get 'page/home'

  # Important: set Devise for the users resource before delcaring
  #   any further user resources
  devise_for :users, controllers: { sessions: 'session/sessions' }

  resources :users do
    collection do
      get 'search'
    end
  end

  root 'page#home'

end
